package cn.tenmg.flink.jobs.launcher;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.JobID;
import org.apache.flink.api.common.JobStatus;
import org.apache.flink.client.deployment.StandaloneClusterId;
import org.apache.flink.client.program.PackagedProgram;
import org.apache.flink.client.program.PackagedProgram.Builder;
import org.apache.flink.client.program.PackagedProgramUtils;
import org.apache.flink.client.program.rest.RestClusterClient;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.ConfigurationUtils;
import org.apache.flink.configuration.JobManagerOptions;
import org.apache.flink.runtime.jobgraph.JobGraph;
import org.apache.flink.runtime.jobgraph.SavepointRestoreSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.tenmg.flink.jobs.FlinkJobsLauncher.FlinkJobsInfo.State;
import cn.tenmg.flink.jobs.config.model.FlinkJobs;
import cn.tenmg.flink.jobs.config.model.Operate;
import cn.tenmg.flink.jobs.launcher.context.FlinkJobsLauncherContext;
import cn.tenmg.flink.jobs.launcher.utils.Sets;

/**
 * REST缇ら泦瀹㈡埛绔痜link-jobs鍚姩鍣ㄣ�傜敤浜庤繙绋嬫彁浜link浠诲姟
 * 
 * @author June wjzhao@aliyun.com
 * 
 * @since 1.1.4
 */
public class RestClusterClientFlinkJobsLauncher extends AbstractFlinkJobsLauncher {

	private static final Logger log = LoggerFactory.getLogger(RestClusterClientFlinkJobsLauncher.class);

	private static final Queue<Configuration> configurations = new LinkedList<Configuration>();

	private static final int COUNT;

	private static final Set<String> localOperates = Sets.as("Jdbc");

	static {
		Configuration configuration = ConfigurationUtils
				.createConfiguration(FlinkJobsLauncherContext.getConfigProperties());
		String rpcServers = FlinkJobsLauncherContext.getProperty("jobmanager.rpc.servers");
		if (StringUtils.isBlank(rpcServers)) {
			configurations.add(configuration);
		} else {
			Configuration config;
			String servers[] = rpcServers.split(","), server[];
			for (int i = 0; i < servers.length; i++) {
				config = configuration.clone();
				server = servers[i].split(":", 2);
				config.set(JobManagerOptions.ADDRESS, server[0].trim());
				if (server.length > 1) {
					config.set(JobManagerOptions.PORT, Integer.parseInt(server[1].trim()));
				} else if (!config.contains(JobManagerOptions.PORT)) {
					config.set(JobManagerOptions.PORT, 6123);
				}
				configurations.add(config);
			}
		}
		COUNT = configurations.size();
	}

	@Override
	public FlinkJobsInfo launch(FlinkJobs flinkJobs) throws Exception {
		RestClusterClient<StandaloneClusterId> client = null;
		try {
			Map<String, String> options = flinkJobs.getOptions();
			String classpaths = FlinkJobsLauncherContext.getProperty("classpaths"),
					parallelism = FlinkJobsLauncherContext.getProperty("parallelism.default", "1");
			SavepointRestoreSettings savepointRestoreSettings = SavepointRestoreSettings.none();
			if (options != null && !options.isEmpty()) {
				if (options.containsKey("classpaths")) {
					classpaths = options.get("classpaths");
				}
				if (options.containsKey("parallelism")) {
					parallelism = options.get("parallelism");
				}
				if (options.containsKey("fromSavepoint")) {
					String savepointPath = options.get("fromSavepoint");
					if (options.containsKey("allowNonRestoredState")) {
						savepointRestoreSettings = SavepointRestoreSettings.forPath(savepointPath,
								"true".equals(options.get("allowNonRestoredState")));
					} else {
						savepointRestoreSettings = SavepointRestoreSettings.forPath(savepointPath);
					}
				}
			}
			Configuration configuration = getConfiguration();
			Builder builder = PackagedProgram.newBuilder().setConfiguration(configuration)
					.setEntryPointClassName(getEntryPointClassName(flinkJobs)).setJarFile(new File(getJar(flinkJobs)))
					.setUserClassPaths(toURLs(classpaths)).setSavepointRestoreSettings(savepointRestoreSettings);

			String arguments = getArguments(flinkJobs);
			if (!isEmptyArguments(arguments)) {
				builder.setArguments(arguments);
			}
			boolean submit;
			if (flinkJobs.getServiceName() == null) {
				submit = false;
				List<Operate> operates = flinkJobs.getOperates();
				if (operates != null) {
					for (int i = 0, size = operates.size(); i < size; i++) {
						if (!localOperates.contains(operates.get(i).getType())) {
							submit = true;
							break;
						}
					}
				}
			} else {
				submit = true;
			}

			PackagedProgram packagedProgram = builder.build();
			boolean suppressOutput = Boolean.valueOf(FlinkJobsLauncherContext.getProperty("suppress.output", "false"));
			if (submit) {
				JobGraph jobGraph = PackagedProgramUtils.createJobGraph(packagedProgram, configuration,
						Integer.parseInt(parallelism), suppressOutput);
				Properties customConf = toProperties(flinkJobs.getConfiguration());
				client = getRestClusterClient(configuration, customConf);
				for (int i = 0; i < COUNT; i++) {
					try {
						return submitJob(client, jobGraph);
					} catch (Exception e) {
						if (client != null) {
							client.close();
						}
						if (i < COUNT) {
							log.error("Try to submit job fail", e);
							client = getRestClusterClient(getConfiguration(), customConf);// try next
						} else {
							throw e;
						}
					}
				}
				return null;
			} else {
				final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
				Thread.currentThread().setContextClassLoader(packagedProgram.getUserCodeClassLoader());
				final PrintStream originalOut = System.out;
				final PrintStream originalErr = System.err;
				final ByteArrayOutputStream stdOutBuffer;
				final ByteArrayOutputStream stdErrBuffer;
				if (suppressOutput) {
					// temporarily write STDERR and STDOUT to a byte array.
					stdOutBuffer = new ByteArrayOutputStream();
					System.setOut(new PrintStream(stdOutBuffer));
					stdErrBuffer = new ByteArrayOutputStream();
					System.setErr(new PrintStream(stdErrBuffer));
				} else {
					stdOutBuffer = null;
					stdErrBuffer = null;
				}
				try {
					packagedProgram.invokeInteractiveModeForExecution();
				} finally {
					if (suppressOutput) {
						System.setOut(originalOut);
						System.setErr(originalErr);
					}
					Thread.currentThread().setContextClassLoader(contextClassLoader);
				}
				return null;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (client != null) {
				client.close();
			}
		}
	}

	@Override
	public String stop(String jobId) throws Exception {
		RestClusterClient<StandaloneClusterId> client = null;
		try {
			client = getRestClusterClient(getConfiguration(), null);
			return client.stopWithSavepoint(JobID.fromHexString(jobId), false,
					FlinkJobsLauncherContext.getProperty("state.savepoints.dir")).get();
		} catch (Exception e) {
			throw e;
		} finally {
			if (client != null) {
				client.close();
			}
		}
	}

	private static FlinkJobsInfo submitJob(RestClusterClient<StandaloneClusterId> client, JobGraph jobGraph)
			throws Exception {
		CompletableFuture<JobID> submited = client.submitJob(jobGraph);
		JobID jobId = submited.get();
		FlinkJobsInfo appInfo = new FlinkJobsInfo();
		appInfo.setJobId(jobId.toString());
		JobStatus jobStatus = client.getJobStatus(jobId).get();
		if (JobStatus.INITIALIZING.equals(jobStatus)) {
			appInfo.setState(State.ACCEPTED);
		} else if (JobStatus.RUNNING.equals(jobStatus)) {
			appInfo.setState(State.RUNNING);
		} else if (JobStatus.FINISHED.equals(jobStatus)) {
			appInfo.setState(State.FINISHED);
		} else if (JobStatus.FAILED.equals(jobStatus)) {
			appInfo.setState(State.FAILED);
		} else if (JobStatus.CANCELED.equals(jobStatus)) {
			appInfo.setState(State.KILLED);
		} else {
			appInfo.setState(State.SUBMITTED);
		}
		return appInfo;
	}

	private static synchronized Configuration getConfiguration() {
		Configuration configuration = configurations.poll();
		configurations.add(configuration);
		return configuration;
	}

	private RestClusterClient<StandaloneClusterId> getRestClusterClient(Configuration configuration,
			Properties customConf) throws Exception {
		if (customConf != null) {
			configuration = configuration.clone();
			configuration.addAll(ConfigurationUtils.createConfiguration(customConf));
		}
		return newRestClusterClient(configuration);
	}

	private RestClusterClient<StandaloneClusterId> newRestClusterClient(Configuration configuration) throws Exception {
		return new RestClusterClient<StandaloneClusterId>(configuration, StandaloneClusterId.getInstance());
	}

	private static List<URL> toURLs(String classpaths) throws MalformedURLException {
		if (classpaths == null || "".equals(classpaths.trim())) {
			return Collections.emptyList();
		} else {
			String[] paths;
			if (classpaths.contains(";")) {
				paths = classpaths.split(";");
			} else {
				paths = classpaths.split(",");
			}
			List<URL> urls = new ArrayList<URL>();
			for (int i = 0; i < paths.length; i++) {
				urls.add(new URL(paths[i].trim()));
			}
			return urls;
		}
	}

	private static Properties toProperties(String configuration) throws IOException {
		Properties properties = null;
		if (configuration != null) {
			properties = new Properties();
			properties.load(new StringReader(configuration));
		}
		return properties;
	}

}