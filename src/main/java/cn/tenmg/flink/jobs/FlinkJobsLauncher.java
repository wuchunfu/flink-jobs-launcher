package cn.tenmg.flink.jobs;

import cn.tenmg.flink.jobs.config.model.FlinkJobs;

/**
 * flink-jobs应用程序启动器
 * 
 * @author June wjzhao@aliyun.com
 * 
 * @since 1.0.0
 */
public interface FlinkJobsLauncher {

	/**
	 * 启动flink-jobs应用程序
	 * 
	 * @param flinkJobs
	 *            flink-jobs配置对象
	 * @throws Exception
	 *             发生异常
	 */
	FlinkJobsInfo launch(FlinkJobs flinkJobs) throws Exception;

	/**
	 * 停止flink-jobs应用程序
	 * 
	 * @param jobId
	 *            flink作业编号
	 * @return 保存点位置
	 * @throws Exception
	 */
	String stop(String jobId) throws Exception;

	/**
	 * flink-jobs应用程序信息
	 * 
	 * @author June wjzhao@aliyun.com
	 * 
	 * @since 1.1.0
	 */
	public static interface FlinkJobsInfo {

		/**
		 * 获取任务编号
		 * 
		 * @return 返回任务编号
		 */
		String getJobId();

		/**
		 * 获取应用状态
		 * 
		 * @return 返回应用状态
		 */
		State getState();

		/**
		 * 获取运行信息
		 * 
		 * @return 运行信息
		 */
		String getMessage();

		public enum State {
			/**
			 * 已提交
			 */
			SUBMITTED,
			/**
			 * 已接受
			 */
			ACCEPTED,
			/**
			 * 正在运行
			 */
			RUNNING,
			/**
			 * 已完成
			 */
			FINISHED,
			/**
			 * 失败的
			 */
			FAILED,
			/**
			 * 被杀死
			 */
			KILLED
		}

	}

}
