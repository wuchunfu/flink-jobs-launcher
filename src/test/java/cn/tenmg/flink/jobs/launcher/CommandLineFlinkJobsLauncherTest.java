package cn.tenmg.flink.jobs.launcher;

import com.alibaba.fastjson.JSON;

import cn.tenmg.flink.jobs.FlinkJobsLauncher.FlinkJobsInfo;
import cn.tenmg.flink.jobs.config.loader.XMLConfigLoader;
import cn.tenmg.flink.jobs.config.model.FlinkJobs;
import cn.tenmg.flink.jobs.launcher.CommandLineFlinkJobsLauncher.Action;

/**
 * CommandLineFlinkJobsLauncher测试
 * 
 * @author June wjzhao@aliyun.com
 * 
 * @since 1.1.4
 *
 */
public class CommandLineFlinkJobsLauncherTest {

	public static void main(String[] args) throws Exception {
		FlinkJobs flinkJobs = XMLConfigLoader.getInstance()
				.load(CommandLineFlinkJobsLauncherTest.class.getClassLoader().getResourceAsStream("WordCount.xml"));
		CommandLineFlinkJobsLauncher launcher = new CommandLineFlinkJobsLauncher();
		launcher.setFlinkHome("D:\\Programs\\flink-1.8.3");
		launcher.setAction(Action.RUN);
		FlinkJobsInfo flinkJobsInfo = launcher.launch(flinkJobs);

		System.out.println("Flink job launched: " + JSON.toJSONString(flinkJobsInfo));// 启动flink-jobs作业
		Thread.sleep(80000);
		System.out.println("Flink job of jobId: " + flinkJobsInfo.getJobId() + " stopped, savepoint path: "
				+ launcher.stop(flinkJobsInfo.getJobId()));// 停止flink-jobs作业
	}

}