package cn.tenmg.flink.jobs.launcher;

import com.alibaba.fastjson.JSON;

import cn.tenmg.flink.jobs.config.loader.XMLConfigLoader;
import cn.tenmg.flink.jobs.config.model.FlinkJobs;
import cn.tenmg.flink.jobs.launcher.AbstractFlinkJobsLauncher.FlinkJobsInfo;

/**
 * RestClusterClientFlinkJobsLauncher测试
 * 
 * @author June wjzhao@aliyun.com
 * 
 * @since 1.1.4
 *
 */
public class RestClusterClientFlinkJobsLauncherTest {

	public static void main(String[] args) throws Exception {
		RestClusterClientFlinkJobsLauncher launcher = new RestClusterClientFlinkJobsLauncher();
		FlinkJobs flinkJobs = XMLConfigLoader.getInstance().load(
				RestClusterClientFlinkJobsLauncherTest.class.getClassLoader().getResourceAsStream("WordCount.xml"));
		FlinkJobsInfo flinkJobsInfo = launcher.launch(flinkJobs);
		System.out.println("Flink job launched: " + JSON.toJSONString(flinkJobsInfo));// 启动flink-jobs作业
		Thread.sleep(80000);
		System.out.println("Flink job of jobId: " + flinkJobsInfo.getJobId() + " stopped, savepoint path: "
				+ launcher.stop(flinkJobsInfo.getJobId()));// 停止flink-jobs作业
	}
}
